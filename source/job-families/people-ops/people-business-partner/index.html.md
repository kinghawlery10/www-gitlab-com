---
layout: job_family_page
title: "People Business Partner"
---

The People Business Partner (PBP) position is responsible for aligning business objectives with employees and management in designated business units. In many companies, the role is referred to as HRBP. The position serves as a consultant to management on human resource-related issues. The successful HRBP acts as an employee champion and change agent. The position formulates partnerships across the People Ops function to deliver value-added service to management and team members that reflects the business objectives of the organization. The position will include international human resource responsibilities. The HRBP maintains an effective level of business literacy about the business unit's financial position, its midrange plans, its culture, and its competition. This role is intended to be solution-focused, and partner with and advise the business, without being “the HR police.”

### Director, HRBP

## Responsibilities

* Forms effective relationships with the client groups and consults with executive level management, providing People guidance when appropriate.
* Mentorship and coaching for People Ops Team Members.
* Ability to identify great talent, internally and externally, who will raise the bar on the team.
* Analyzes trends and metrics in partnership with the People Ops group to develop solutions, programs, and opportunities for learning
* Manages and resolves complex employee relations issues. Conducts effective, thorough and objective investigations.
* Maintains in-depth knowledge of legal requirements related to day-to-day management of team members, reducing legal risks and ensuring regulatory compliance. Partners with the legal department as needed/required.
* Works closely with management and employees to improve work relationships, build morale, and increase productivity and retention.
* Partners with team members globally to ensure a vibrant and effective workplace.
* Provides guidance and input on business unit restructures, workforce planning and succession planning to support the business strategy.
* Identifies training needs for business units and individual executive coaching needs. Participates in evaluation and monitoring of training programs to ensure success. Follows up to ensure training objectives are met.

## Requirements

* Minimum of 10 years experience as an HR Business Partner, with at least 5 years of experience supporting Senior Executive and/or C-level leaders. 
* Strong attention to detail and ability to work well with fluid information.
* Comfortable using technology.
* Effective and concise verbal and written communication skills with the ability to collaborate with cross-functional team members. At times, courage is required to respectfully speak up, “push back”, challenge “the way things have always been done” and even disagree with leaders and executives.
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem-solving
* Have implemented various learning and development programs that are aligned to the business needs
* Experience working with Global Teams
* Very strong EQ, with fine-tuned instincts and problem-solving skills.
* Strong drive and initiative with the ability to self-manage.

### Senior HRBP

## Responsibilities

* Provide strategic HR support to client groups and consults with VPs, Directors and Managers in areas of performance management, management training and coaching, organizational development, and employee relations.
* Provides HR policy guidance and interpretation.
* Manages and resolves complex employee relations issues. Conducts effective, thorough and objective investigations.
* Work closely with HR colleagues in implementing organization wide initiatives.
* Analyze trends and metrics in partnership with HR group to develop solutions, programs and policies.
* Raise concerns or trends to the CCO, legal, and/or HRBP directors to address timely and effectively.
* Being an enthusiastic team player with a strong drive to create a positive work environment.
* The ability to be comfortable with high volume workload and not be afraid to "roll up your sleeves".
* Experience working with Global teams. 


## Requirements

* 5 years of experience as an HR Business Partner supporting front line, mid and senior-level leaders.
* Strong attention to detail and ability to work well with fluid information
* Comfortable using technology.
* Effective and concise verbal and written communication skills with the ability to collaborate with cross-functional team members
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem-solving
* Very strong EQ, with fine-tuned instincts and problem-solving skills.

### Intermediate HRBP

## Responsibilities

* Forms effective relationships with the client groups and consults with line management, providing People guidance when appropriate.
* Analyzes trends and metrics in partnership with the People Ops group to develop solutions, programs, and opportunities for learning
* Manages and resolves complex employee relations issues. Conducts effective, thorough and objective investigations.
* Maintains in-depth knowledge of legal requirements related to day-to-day management of employees, reducing legal risks and ensuring regulatory compliance. Partners with the legal department as needed/required.
* Works closely with management and employees to improve work relationships, build morale, and increase productivity and retention.
* Partners with colleagues outside the US to ensure a vibrant and effective workplace.
* Provides guidance and input on business unit restructures, workforce planning and succession planning.
* Identifies training needs for business units and individual executive coaching needs.
* Participates in evaluation and monitoring of training programs to ensure success. Follows up to ensure training objectives are met.

## Requirements

* 3 years of HR in recruiting, employee relations, and/or organizational development.
* Demonstrate discretion and sound judgment while working with sensitive and confidential materials.
* Comfortable using technology.
* Effective verbal and written communications.
* Passion for results.
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment.
* Resourceful and takes initiative to seek internal and external resources when problem-solving.
* Ability to solve moderate to complex problems
* Experience working with Global Talent in areas like Europe, India, and China
* Very strong EQ, with fine tuned instincts and problem solving skills.

## Specialties

### People Business Partner, Engineering:
* 5-10 years HRBP support in a high growth, fast moving company where ongoing change is the norm, with at least 2-3 of those years supporting a SaaS or related software organization.
* Partner with the Engineering VP on the people strategy for their organization.
* Provide support and guidance to people leaders at all levels within the Engineering organization (front line manager to director).
* Understand GitLab’s Engineering strategy in order to align people strategies to meet business goals.
* Support the Engineering team through all People Ops processes including compensation, talent development, performance management, employee relations, change management and organizational design.
* Analytically driven, experience  in utilizing qualitative and quantitative approaches to problem solving and root cause analysis. 
* Drive equality, diversity, and inclusion throughout all of our programs and initiatives.

### People Business Partner, Sales:
* 5-10 years HRBP support in a high growth, fast moving company where ongoing change is the norm, with at least 2-3 of those years supporting a global software sales organization with particular focus on Enterprise Sales, Commercial Sales and Customer Success. 
* Partner with 1 or more Sales VPs on the people strategy for their organization  
* Provide support and guidance to people leaders at all levels within the sales organization (front line manager to director)
* Experience with sales compensation, sales plans, sales data/analysis as needed, in partnership with the Sales Operations and People Operations team.
* Understand GitLab’s sales strategy in order to align people strategies to meet business goals.
* Drive equality, diversity, and inclusion throughout all of our programs and initiatives. 

