---
layout: markdown_page
title: "Design: self service 301s on about.gitlab.com"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Resources

Blueprint: [Self service 301s on about.gitlab.com]

Issues:

* https://gitlab.com/gitlab-com/www-gitlab-com/issues/3952
* https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6432
* https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6433



