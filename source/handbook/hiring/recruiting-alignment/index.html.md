---
layout: markdown_page
title: "Recruiting Alignment"
---

## Recruiter, Coordinator and Sourcer Alignment by Department

| Department                    | Recruiter       | Coordinator     |Sourcer     |
|--------------------------|-----------------|-----------------|-----------------|
| Board of Directors          | April Hoffbauer   | | |
| Executive          | April Hoffbauer    | Emily Mowry | Anastasia Pshegodskaya |
| Enterprise Sales, North America | TBH   | Taharah Nix |Susan Hill        |
| Commercial Sales,	NA/EMEA/APAC | Marcus Carter    | Chantal Rollison |Susan Hill/Viren Rana - APAC        |
| Field Operations,	NA/EMEA/APAC | TBH   | Taharah Nix |Susan Hill/Viren Rana - APAC        |
| Customer Success, EMEA | D.H.  | Bernadett Gal |Kanwal Matharu |
| Federal Sales, Customer Success, Marketing | Stephanie Kellert   | Kike Adio |Viren Rana |
| Marketing, North America | Steph Sarff   | Chantal Rollison |Viren Rana |
| Marketing, EMEA | Sean Delea   | Kike Adio |Viren Rana |
| G&A | Stephanie Garza   | Emily Mowry |Kanwal Matharu |
| Quality                   | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| UX                        | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Technical Writing         | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Support                   | Cyndi Walsh                                             | Chantal Rollison      | Alina Moise      |
| Security                  | Cyndi Walsh                                             | Chantal Rollison      | Zsusanna Kovacs      |
| Infrastructure            | Matt Allen                                              | Emily Mowry      | Chris Cruz |
| Development - Dev         | Catarina Ferreira                                       | Chantal Rollison        | Chris Cruz       |
| Development - Secure/Defend      | Liam McNally                                            | Lea Hanopol        | Alina Moise       |
| Development - Ops & CI/CD  | Eva Petreska                                            | Taharah Nix      | Zsuzsanna Kovacs      |
| Development - Enablement  | Trust Ogor                                              | Bernadett Gal        | Alina Moise       |
| Development - Growth      | Trust Ogor                                              | Bernadett Gal        | Alina Moise       |
| Engineering Leadership                | Steve Pestorich                                         | Lea Hanopol      |  Anastasia Pshegodskaya |
| Product Management  | Matt Allen                      | Emily Mowry |  Anastasia Pshegodskaya/Chris Cruz |
