---
layout: markdown_page
title: "Technical Writing Team"
---

## On this page
{:.no_toc}

* Will be replaced with the ToC, excluding the "On this page" header
{:toc}

## About Us

The primary goal of the Technical Writing team is to continuously develop GitLab's
product documentation content to meet the evolving needs of all users and administrators.

Documentation is intended to educate readers about features and best practices,
and to enable them to efficiently configure, use, and troubleshoot GitLab. To this end, the
team also manages the [docs.gitlab.com](https://docs.gitlab.com) site and related process and tooling.

Our team comprises:

- A [technical writing manager](../../../job-families/product/technical-writing-manager/).
- A group of [technical writers](../../../job-families/product/technical-writer/).

Technical writers partner with anyone in the GitLab community who is concerned with
documentation, especially developers, who are the first to update docs for the
GitLab features that they code.

The technical writing team Slack channel is [#docs-team](https://gitlab.slack.com/messages/CCB575BGT) (private to GitLab Inc.).

## Projects

The technical writing team are:

- Authors, reviewers, and maintainers of documentation for many [engineering projects](../../engineering/projects/).
- Assigned across the [DevOps stages](../categories/) of the product.

For more information on documentation at GitLab, see:

- The [Documentation](../../documentation/) section of the Handbook.
- [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/) in the contributor documentation.

## Responsibilities

The team is broadly responsible for the following at GitLab.

### Content

[Documentation Content](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Documentation&label_name[]=epic-level-1&search=Content), including:

- [Developing new content](https://gitlab.com/groups/gitlab-org/-/epics/774) to meet the needs of the community.
- [Reviewing and collaborating on documentation plans](https://gitlab.com/groups/gitlab-org/-/epics/776), reviewing doc merge requests or recently merged docs, and ensuring that content meets style and language standards.
- [Reorganizing, revamping, and authoring improved content](https://gitlab.com/groups/gitlab-org/-/epics/775) to ensure completeness and a smooth user experience.

### Publishing

[Documentation Site](https://gitlab.com/groups/gitlab-com/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Documentation) (docs.gitlab.com) including
maintaining and enhancing the documentation site’s:

- Architecture.
- Design.
- Automation.
- Versioning.
- Search.
- SEO.
- Feedback methods.
- Analytics.

### Processes

[Documentation Process](https://gitlab.com/groups/gitlab-org/-/epics/779), including:

- Ensuring that processes are in place and being followed to keep the GitLab docs up to date.
- Following and optimizing documentation workflows with Product and Engineering, Documentation Team workflows, and the division of work.
- Triaging doc-related issues.
- Refining the Documentation Style Guide and continuously improving content about GitLab documentation and its contribution process.
- Making it easier for anyone to contribute to the documentation while efficiently handling community contributions to docs.

### Collaboration

[Collaboration](https://gitlab.com/groups/gitlab-org/-/epics/779), including:

- Working on documentation efforts with Product, Support, Marketing, Engineering, Community Marketing, other GitLab teams, and the wider GitLab community.
- Ensuring that relevant documentation is easily accessible from within the product.
- Acting as reviewers of the monthly [release post](../../handbook/marketing/blog/release-posts/).

This work is sorted into the [top-level Documentation epics](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Documentation&label_name%5B%5D=epic-level-1&scope=all&sort=created_asc&state=opened&utf8=%E2%9C%93)
linked above.

## Documentation process

As a remote team, it's important that the Technical Writing team and other documentation contributors follow a standard process.

For more information on GitLab's documentation process, see:

- [Technical Writing workflow](workflow/) in the handbook.
- [Documentation workflows](https://docs.gitlab.com/ee/development/documentation/workflow.html) in the contributor documentation.

## FY20 Vision

- [New Content Development](https://gitlab.com/groups/gitlab-org/-/epics/774) - Leading the planning and authoring of new content to better meet user needs. This will be a significantly increased proportion of the team’s work, and will make use of improved methods and standards.
  - Authoring docs that better address use cases with A-to-Z process flows, going beyond existing feature references and procedures that can be difficult to adapt to real-world use cases. Supported by [updated standards for content types and structure](https://gitlab.com/gitlab-com/Product/issues/74).
  - [Getting Started](https://gitlab.com/groups/gitlab-org/-/epics/782) - a.k.a. unboxing or onboarding docs for various audiences and scenarios.
- [Content Improvement](https://gitlab.com/groups/gitlab-org/-/epics/775):
  - Revamping existing content pages/sections, so that they meet our latest standards, including contextual information on every page (what, why, who).
  - Improve use of documentation feedback/sensing mechanisms. Improved sourcing and channeling/triage; making use of existing comments and issues, while bringing in new sources including search data, surveys, user testing, etc.
  - [Improve rate of contributions to docs as SSOT](https://gitlab.com/gitlab-com/Product/issues/51) and using ‘docs-first’ methods by tracking contributions and demonstrating best practices.
- Docs UX:
  - Next iteration of [docs versioning](https://gitlab.com/gitlab-org/gitlab-docs/issues/248).
  - [Removing documentation from /help](https://gitlab.com/groups/gitlab-org/-/epics/693), linking instead from UI to versioned links on docs.gitlab.com.
