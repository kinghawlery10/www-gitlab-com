---
layout: markdown_page
title: "Growth Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Growth
{: #welcome}

 Growth team is responsible to foster a scientific approach to expanding the value, monetization, and customer experience of the product. This means with the needed tracking in place, the growth team models what drives metric changes based on user behavior and based on the gathered insights delivering changes to the product.

* I have a question. Who do I ask?

Questions should start by @ mentioning the Product Manager for the [Growth product
category](/handbook/product/categories/#growth) or create an issue in [https://gitlab.com/gitlab-org/growth](https://gitlab.com/gitlab-org/growth).

### How we work
* We're data savvy
* In accordance with our [GitLab values](https://about.gitlab.com/handbook/values/)
* Transparently: nearly everything is public
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos


### Links and resources
{: #links}
* Collection of ideas that we explore
  * [Growth opportunities](https://gitlab.com/gitlab-org/growth/product)
  * For the exploration and analyses of opportunities we use [Looker](https://gitlab.looker.com) and [Jupyter notebooks](https://gitlab.com/gitlab-org/growth-notebooks)
* Our Slack channel - [#g_growth](https://gitlab.slack.com/archives/CDLCBGEDV/)
* Meeting agendas
  * Agendas and notes from team meetings can be found in [this Google Doc](https://docs.google.com/document/d/1ynp-R-aJdnn1V74zqG_Lteh_v1KlUkn_5yIydYFNLwk). For transparency across the team, we use one agenda document.
