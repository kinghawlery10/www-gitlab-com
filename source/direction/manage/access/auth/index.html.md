---
layout: markdown_page
title: "Category Vision - Authorization and Authentication"
---

- TOC
{:toc}

| Category Attribute | Link | 
| ------ | ------ |
| [Stage](https://about.gitlab.com/handbook/product/categories/#hierarchy) | [Manage](https://about.gitlab.com/direction/manage) | 
| [Group](https://about.gitlab.com/handbook/product/categories/#hierarchy) | [Control](https://about.gitlab.com/direction/manage/control) | 
| [Maturity](/direction/maturity/#maturity-plan) | [Minimal](#maturity) |
| Labels | [authentication](https://gitlab.com/groups/gitlab-org/-/epics?label_name=authentication), [authorization](https://gitlab.com/groups/gitlab-org/-/epics?label_name=authorization), [oauth](https://gitlab.com/groups/gitlab-org/-/epics?label_name=oauth), [ldap](https://gitlab.com/groups/gitlab-org/-/epics?label_name=ldap), [saml](https://gitlab.com/groups/gitlab-org/-/epics?label_name=saml) |


## Authorization and authentication

Authentication and authorization are critical, foundational elements to keeping resources secure but accessible. This is of particular importance for large enterprises or companies operating in regulated environments; for an authentication strategy to function, it must be secure and scalable. Onboarding/offboarding new employees should be automatable and not allow unauthorized users to access sensitive information.

Furthermore, security strategies have evolved from one of assumed trust within a trusted network. Historically, most organizations assume that authenticated users coming from a trusted source are allowed to access resources - those outside, if not on a VPN, cannot. However, security perimeters have become increasingly porous and an assumed trust model begins to break down with more services shifting to cloud and more employees BYODing or working remotely. 

Our strategy for this year is to aggressively improve the authentication and authorization strategies we currently support, including SAML, LDAP, OAuth, and 2FA.

## Target audience and experience

The primary audience for future effort are administrators in medium to large enterprises. These are privileged, sophisticated users in companies managing employee identities with a single source of truth; this may be a series of LDAP servers or an IdAAS service like Okta. Automation matters - we should minimize the amount of manual work needed to onboard/offboard an employee and be able to assign permissions automatically - but the must-have is security, especially in sensitive environments operating under regulatory scrutiny. We need to strike a balance between being overly permissive (e.g. unintentionally allowing access to an offboarded employee) and overly restrictive (e.g. getting in the way of a developer's workflow and annoying them with reauthentication requests).

## Maturity

GitLab's authentication and authorization is **viable** and nearing **complete**.

## How you can help

As with any category in GitLab, it's dependent on your ongoing feedback and contributions. Here's how you can help:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/628).
1. Find issues in this category accepting merge requests. [Here's an example query](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=authentication&label_name[]=Accepting%20merge%20requests)!

## What's next & why

### Current

While we added an MVC for SAML single sign-on for groups, further iteration is needed. In particular, we need to introduce automated provisioning to make the feature more maintainable (by removing manual effort), and we must add additional layers of security to ensure that an organization's protected resources remain secure - like enforcing the use of SSO.

Please see the [Secure SSO](https://gitlab.com/groups/gitlab-org/-/epics/731) epic.

### Next

Some organizations use a [physical token](https://en.wikipedia.org/wiki/Smart_card) to serve as in-person physical identification and to authenticate with software systems. We've added an [MVC](https://gitlab.com/gitlab-org/gitlab-ee/issues/726) in 11.6, but this MVC does not authenticate against systems like LDAP or support SSH. We'd like to support smart card authentication for all GitLab services, so a user can use their token as identification for any GitLab activity.

Please see the [smart card authentication](https://gitlab.com/groups/gitlab-org/-/epics/279) epic.

## Top user issue(s)

TBD

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

TBD
